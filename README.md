# Performance of Neural Networks for Constitutive Relations

This repo is testing some different ways of using a neural network for a stress-strain relation.


## Requirements

[CRIKit] must be installed before using this repo.


## Running

There is a `.petscrc` file in the root directory, so it is recommended to run the code from the root directory.

- `python src/main.py` runs an elastic simulation.
- `python src/plasticity.py` runs an elasto-plastic example.

Run with `--help` to see more info.

[CRIKit]: https://gitlab.com/crikit/crikit
