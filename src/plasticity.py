import tensorflow
from crikit import *
from crikit.cr.space_builders import DirectSum
from pyadjoint.overloaded_type import create_overloaded_object as coo

import argparse
import crikit
import crikit.utils as utils
import jax
import jax.numpy as jnp
import matplotlib.pyplot as plt
import numpy as onp

from cr import ParamsCR, ParamsCRUFL, ParamsInterface, PlasticityCR, SPDIntegratorCR, UFLPlasticity
from experiment import PlasticityCRExperiment, RobinBC
from main import make_stiffness_network, make_energy_network_cr
from network_builders import FICNN, FIQCNN, MLP, PositiveMonotonicNN
from utils import print_taylor_results

import logging
ffc_logger = logging.getLogger('FFC')
ffc_logger.setLevel(logging.WARNING)

def get_L_from_vec(L_vec, L_idx=((0, 1, 1, 2), (0, 1, 0, 2))):
    L = jnp.zeros((3,3))
    L = jax.ops.index_update(L, L_idx, L_vec)
    return L

def get_tensor_from_voigt(voigt):
    return jnp.array([[voigt[0], voigt[2]],
                      [voigt[2], voigt[1]]])

def get_voigt_from_tensor(tensor):
    return jnp.array([tensor[0,0], tensor[1,1], tensor[0, 1]])

def make_plastic_energy_cr(ex, seed):
    delta_cr = make_energy_network_cr(ex, seed, vmap=False)

    def delta_stress_func(strain, old_strain, old_stress, *network_params):
        strain_tens, old_strain_tens, old_stress_tens = (get_tensor_from_voigt(s) for s in (strain, old_strain, old_stress))
        delta_stress_tens = delta_cr(strain_tens, old_strain_tens, old_stress_tens, params=network_params)
        delta_stress = get_voigt_from_tensor(delta_stress_tens)
        return delta_stress

    cr = make_plastic_cr(delta_stress_func, delta_cr.params, delta_cr.params_bounds)
    return cr

def make_plastic_stiffness_network_cr(ex, seed, **kwargs):
    num_inputs = 9
    G_func, overloaded_network_params, bounds = make_stiffness_network(num_inputs, ex.dim, seed, **kwargs)

    def delta_stress_func(strain, old_strain, old_stress, *network_params):
        network_inputs = jnp.concatenate((strain, old_strain, old_stress))
        G = G_func(network_inputs, *network_params)
        delta_strain = strain - old_strain
        delta_stress = G @ delta_strain
        return delta_stress

    cr = make_plastic_cr(delta_stress_func, overloaded_network_params, bounds)
    return cr

def make_plastic_cr(delta_stress_func, delta_stress_params, delta_stress_bounds):
    """This builds a plastic CR where the output is an average of a linear CR and the given delta-stress JAX function (non-vmapped).

    delta_stress_func will be given current_strain, old_strain, old_stress, and params. It should output a change in stress.
    """

    # L_vec = coo(jnp.array([5, 5, 1, 5]))
    L_vec = coo(jnp.array([10.67521025, 10., 3.73632359, 6.08580619]))
    estimated_yield_stress = 0.7
    def D_func(old_stress):
        x = 10 * ((old_stress/estimated_yield_stress)**2 - 1)
        return jax.scipy.special.expit(x)

    coercive_epsilon = 1
    def plastic_func(strain, old_strain, old_stress, L_vec, *delta_stress_params):
        L = get_L_from_vec(L_vec)

        delta_strain = strain - old_strain
        delta_stress_elastic = L @ (L.T @ delta_strain)

        delta_stress_plastic = delta_stress_func(strain, old_strain, old_stress, *delta_stress_params)

        D = D_func(old_stress)
        stress = old_stress + (1 - D)*delta_stress_elastic + D*delta_stress_plastic + coercive_epsilon * delta_strain
        return stress
    vmap_axes = [0, 0, 0, None] + [None]*len(delta_stress_params)
    plastic_func = jax.vmap(plastic_func, in_axes=vmap_axes)
    pointwise = [True, True, True, False] + [False]*len(delta_stress_params)
    plastic_func_overloaded = overload_jax(plastic_func, nojit=False, pointwise=pointwise)

    def cr_func(point):
        return plastic_func_overloaded(*point, *cr.params)

    class CallableParamsCR(ParamsInterface,Callable):
        @property
        def h(self):
            h = [coo(jnp.array(1e1*onp.random.rand(*p.shape))) for p in self.params[:1]]
            h += [coo(jnp.array(1e1*onp.random.rand(*p.shape))) for p in self.params[1:]]
            return h

    s = (-1,3)
    source = DirectSum([JAXArrays(s)]*3)
    target = JAXArrays(s)
    cr = CallableParamsCR(source, target, cr_func)
    cr.update_hidden_state = cr.__call__
    cr.reset_hidden_state = lambda: None

    # Prepend L_vec to the parameter list and set the CR parameters.
    all_params = [L_vec, *delta_stress_params]
    if delta_stress_bounds is not None:
        delta_stress_bounds = [[None, *delta_stress_bounds[0]], [None, *delta_stress_bounds[1]]]
    cr.set_params(all_params, bounds=delta_stress_bounds)
    return cr


def make_ring_experiment(args, ex_kwargs):
    mesh = Mesh("mesh/thick_cylinder.xml")
    ex = PlasticityCRExperiment(mesh, **ex_kwargs)

    # Set up loading limit.
    Re = 1.3
    Ri = 1.   # external/internal radius
    yield_stress = 250.
    lim = float(2/sqrt(3)*ln(Re/Ri) * yield_stress)
    loading = Expression("-q*t", q=lim, t=0, degree=2)

    # Set up boundary condition.
    facets = MeshFunction("size_t", mesh, "mesh/thick_cylinder_facet_region.xml")
    dir_bcs = [DirichletBC(ex.V.sub(1), 0, facets, 1),
               DirichletBC(ex.V.sub(0), 0, facets, 3)]
    n = FacetNormal(mesh)
    outer_surface = ds(domain=mesh, subdomain_data=facets)(4)
    traction = loading * n
    rob_bcs = RobinBC(0, 1, traction, outer_surface)
    ex.set_bcs(dir_bcs, rob_bcs)

    # Set up time-dependent parts.
    load_steps = args.T * (onp.linspace(0, 1, args.Nt+1)[1:]**0.3)
    results = onp.zeros((args.Nt+1, 2))

    def pre_cback(t):
        loading.t = t

    def post_cback(i, t, u):
        # Record results.
        results[i+1, :] = (u(Ri, 0)[0], t)

    ex.set_temporal_parameters(load_steps, pre_cback, post_cback)

    # Set up true CR.
    E = Constant(70)
    nu = Constant(0.3)
    yield_stress = Constant(0.25)
    Et = E * Constant(0.01)
    true_cr = UFLPlasticity(E, nu, yield_stress, tangent_modulus=Et, plasticity_function_space=ex.Q)

    def plot_results():
        plt.plot(results[:, 0], results[:, 1], "-o")
        plt.ylabel(r"Applied pressure")
        plt.xlabel("True displacement of inner boundary")

    return ex, true_cr, loading, plot_results


def make_plate_experiment(args, ex_kwargs):
    mesh = UnitSquareMesh(20, 10)
    ex = PlasticityCRExperiment(mesh, **ex_kwargs)

    # Set up loading limit.
    loading = Expression(("px*sin(t*pi)", "py*sin(t*pi)"), px=-1.6, py=0, t=0, degree=4)

    # Set up boundary condition.
    def left_boundary(x, on_boundary):
        return on_boundary and near(x[0], 0, 1e-5)
    dir_bcs = [DirichletBC(ex.V, (0,0), left_boundary)]

    def right_boundary(x, on_boundary):
        return on_boundary and near(x[0], 1, 1e-5)

    class RightBoundary(SubDomain):
        def inside(self, x, on_boundary):
            return right_boundary(x, on_boundary)

    markers = MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
    RightBoundary().mark(markers, 1)
    dright = ds(1, domain=mesh, subdomain_data=markers)
    rob_bcs = RobinBC(0, 1, loading, dright)
    ex.set_bcs(dir_bcs, rob_bcs)

    # Set up time-dependent parts.
    load_steps = args.T * onp.linspace(0, 1, args.Nt+1)[1:]
    results = onp.zeros((args.Nt+1, 3))

    def pre_cback(i, t):
        print(f'  At time {i}: {t}')
        loading.t = t
    def post_cback(i, t, u):
        dx, dy = u(1,1)
        results[i+1, :] = (t, dx, dy)
    ex.set_temporal_parameters(load_steps, pre_cback, post_cback)

    # Set up true CR.
    E = Constant(100)
    nu = Constant(0.35)
    yield_stress = Constant(0.97)
    H = Constant(10)
    true_cr = UFLPlasticity(E, nu, yield_stress, hardening_modulus=H, plasticity_function_space=ex.Q)

    def plot_results():
        plt.plot(results[:, 0], results[:, 1], "-o", label="dx")
        plt.plot(results[:, 0], results[:, 2], "-o", label="dy")
        plt.xlabel(r"Time")
        plt.ylabel("Displacement of top right point")
        plt.legend()

    return ex, true_cr, loading, plot_results

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    utils.add_bool_arg(parser, 'debug', False, "show Jacobian singular values")
    parser.add_argument("--seed", type=int, help="seed for RNG", default=None)
    parser.add_argument("-T", "--T", type=float, help="Simulation time", default=0.5)
    parser.add_argument("--Nt", type=int, help="Number of time increments", default=5)
    utils.add_bool_arg(parser, 'ufl', False, "Use UFL for true CR instead of CRIKit CR")

    exp_info = (
                ('ring', 'A radial force is applied to the outer edge of a ring to expand it.'),
                ('plate', 'A tensile force is applied to a square plate.'),
               )
    exp_help = 'Selects which experiment to run.\n'
    exp_help += '\n'.join(f"- `{mode}`: {desc}" for mode, desc in exp_info)
    parser.add_argument("--ex", type=str, choices=[m for m,d in exp_info], help=exp_help, default='plate')

    modes = ['jax', 'network']
    parser.add_argument('mode', choices=modes, help='specify which CR type to use', nargs="?", default=None)

    stiff_mat_type_info = (('cholesky', '`C = L @ L.T`'),
                          ('exponential', '`C = exp(L + L.T)`'))
    stiff_mat_type_help = 'Controls how C is generated from L.\n'
    stiff_mat_type_help += '\n'.join(f"- `{mode}`: {desc}" for mode, desc in stiff_mat_type_info)
    parser.add_argument("--stiff_mat_type", type=str, choices=[m for m,d in stiff_mat_type_info], help=stiff_mat_type_help, default='cholesky')

    stiff_mode_info = (('anisotropic', 'L is lower triangular.'),
             ('orthotropic', 'L has a lower triangular `dim x dim` block and the rest is diagonal.'),
             ('full', 'L is dense'))
    stiff_mode_help = 'Controls how L is generated from the network.\n'
    stiff_mode_help += '\n'.join(f"- `{mode}`: {desc}"for mode, desc in stiff_mode_info)
    parser.add_argument("--stiff_mode", type=str, choices=[m for m,d in stiff_mode_info], help=stiff_mode_help, default='orthotropic')
    utils.add_bool_arg(parser, 'stiff_chol_exponential', False, "Use `C = exp(L @ L.T)` instead of `C = L @ L.T`. This ensures positive definiteness.")
    utils.add_bool_arg(parser, "viz_tape", False, "Visualize Pyadjoint tape with TensorBoard")

    utils.add_bool_arg(
        parser,
        "show",
        True,
        "Don't show plots interactively. This is useful when combined with `--save_dir`.",
    )

    parser.add_argument(
        "--save_dir",
        type=str,
        help="Directory to save figures to. By default, the figures won't be saved.",
        default="",
    )

    parser.add_argument(
        "--format",
        type=str,
        help="Image format to use for saving figures.",
        default="png",
    )

    parser.add_argument(
        "--quad",
        type=int,
        help="Quadrature degree used to assemble PDE weak form.",
        default=6,
    )

    utils.add_bool_arg(parser, "verify", False, "Do a Taylor test gradient verification, which can take a lot of time.")

    parser.add_argument("--save_params", type=str, help="File to write params after doing training.", default='params.npy')
    parser.add_argument("--load_params", type=str, help="File to load params from before doing training.", default='')

    args = parser.parse_args()

    onp.random.seed(args.seed)
    tape = get_working_tape()

    if not args.debug:
        set_log_level(crikit.logging.WARNING)

    if args.save_dir != "":
        utils.FIGURE_SAVE_DIR = args.save_dir
        try:
            plt.rc("savefig", format=args.format)
        except Exception as e:
            print("Warning: couldn't set output image format:", e)

    # Create experiment and true CR.
    ex_kwargs = dict(quad_degree=args.quad)
    if args.ex == 'ring':
        ex, true_cr, loading, plot_results = make_ring_experiment(args, ex_kwargs)
    elif args.ex == 'plate':
        ex, true_cr, loading, plot_results = make_plate_experiment(args, ex_kwargs)
        print(f'Running plate experiment with loading {loading.px, loading.py}')
    else:
        raise ValueError('invalid experiment: "%s"' % args.ex)

    # Choose observer and loss function.
    observer = lambda x: x
    loss = integral_loss

    # Generate observations for true CR.
    with stop_annotating():
        us = ex.run(true_cr, ufl=True, disp=args.debug)
        y_trues = [observer(u) for u in us]

    plt.figure()
    plot_results()
    plt.title("Ground-truth single output point")
    utils.saveplot()

    # Plot solution.
    plt.figure()
    f = plot(y_trues[-1], mode="displacement")
    plt.colorbar(f)
    plt.title('Ground-truth experiment full output')
    utils.saveplot()

    if args.mode is None:
        if args.show:
            plt.show()
        return

    if args.mode == 'jax':
        E = coo(jnp.array(1.))
        nu = coo(jnp.array(0.05))
        yield_stress = coo(jnp.array(0.97))
        H = coo(jnp.array(10.))

        E.tf_name = 'E'
        nu.tf_name = 'nu'
        yield_stress.tf_name = 'yield_stress'
        H.tf_name = 'H'

        cr = PlasticityCR(E, nu, yield_stress, hardening_modulus=H)
        params = [E, nu]
        bounds = ([jnp.array(1.), jnp.array(0.)], [jnp.array(1000.), jnp.array(0.5)])
        cr.set_params(params, bounds)
        cr.h = (jnp.array(50e0), jnp.array(1e-1))
    elif args.mode == 'network':
        cr = make_plastic_stiffness_network_cr(ex, args.seed, mat_type=args.stiff_mat_type, mode=args.stiff_mode, chol_exponential=args.stiff_chol_exponential)
    else:
        raise NotImplementedError("Internal error: invalid mode  '%s'" % args.mode)

    if args.load_params != '':
        params_array = onp.load(args.load_params, allow_pickle=False)
        cr.set_params_numpy(params_array)
    print()

    print('     True CR stiffness tensor:')
    print(jnp.array([[float(true_cr.C[i,j]) for j in range(true_cr.C.ufl_shape[1])] for i in range(true_cr.C.ufl_shape[0])]))

    print('Candidate CR stiffness tensor:')
    print(cr.params[0].arr @ cr.params[0].arr.T)

    # Run experiment with candidate CR and compute loss.
    us = ex.run(cr, ufl=False, disp=args.debug)
    y_preds = [observer(u) for u in us]

    J = sum(loss(y_true, y_pred) for y_true, y_pred in zip(y_trues, y_preds))
    Jhat = ReducedFunctional(J, cr.params_controls)

    print()
    print('Initial loss: %g' % J)

    # Plot solution.
    plt.figure()
    f = plot(y_preds[-1], mode="displacement")
    plt.colorbar(f)
    plt.title('Experiment output for candidate CR')
    utils.saveplot()
    # if args.show:
    #     plt.show()
    if args.viz_tape:
        get_working_tape().visualise(launch_tensorboard=True, open_in_browser=True)

    if args.verify:
        # Verify gradient is correct with a Taylor test.
        print()
        epsilons = [1.0 / 2**i for i in range(15)]
        taylor_results = taylor_to_dict(Jhat, cr.params, cr.h, epsilons=epsilons, Hm=0)
        print_taylor_results(taylor_results)

    # Train CR to match observations.
    trained_params = minimize(Jhat, bounds=cr.params_bounds, options=dict(disp=True, gtol=1e-10, ftol=1e-10))

    if args.verify:
        # Verify gradient is correct with a Taylor test.
        print()
        epsilons = [1.0 / 2**i for i in range(15)]
        taylor_results = taylor_to_dict(Jhat, trained_params, cr.h, epsilons=epsilons, Hm=0)
        print_taylor_results(taylor_results)

    # Recompute predicted value and loss with the trained params.
    trained_J = Jhat(trained_params)
    trained_y_pred = Control(y_preds[-1]).tape_value()
    print()
    print('Final loss: %g' % trained_J)
    print('Final params:', trained_params)

    plt.figure()
    f = plot(trained_y_pred, mode="displacement")
    plt.colorbar(f)
    plt.title('Experiment output for trained CR')
    utils.saveplot()

    plt.figure()
    f = plot(y_trues[-1] - trained_y_pred, mode="displacement")
    plt.colorbar(f)
    plt.title('Displacement error (u_true - u_trained)')
    utils.saveplot()

    cr.set_params(trained_params, bounds=cr.params_bounds)

    if args.save_params != '':
        params_array = cr.get_params_numpy()
        onp.save(args.save_params, params_array, allow_pickle=False)

    print()
    print('Candidate CR trained stiffness tensor:')
    print(cr.params[0].arr @ cr.params[0].arr.T)

    if args.show:
        plt.show()


if __name__ == '__main__':
    main()
