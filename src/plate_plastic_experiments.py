import tensorflow
from crikit import *
from crikit.cr.space_builders import DirectSum
from pyadjoint.overloaded_type import create_overloaded_object as coo

import argparse
import crikit
import crikit.utils as utils
import jax
import jax.numpy as jnp
import matplotlib.pyplot as plt
import numpy as onp

from cr import ParamsCR, ParamsCRUFL, ParamsInterface, PlasticityCR, SPDIntegratorCR, UFLPlasticity
from experiment import PlasticityCRExperiment, RobinBC
from main import make_stiffness_network
from network_builders import FICNN, FIQCNN, MLP, PositiveMonotonicNN
from plasticity import make_plastic_stiffness_network_cr, make_plastic_energy_cr, make_plate_experiment, get_L_from_vec
from utils import print_taylor_results

import logging
ffc_logger = logging.getLogger('FFC')
ffc_logger.setLevel(logging.WARNING)

def get_C_from_vec(L_vec):
    L = get_L_from_vec(L_vec)
    C = L @ L.T
    return C

def optimize_params(Jhat, display_params=False, write_C=False, **kwargs):
    record_file = kwargs.pop("record_file", "data.csv")
    old_callback = kwargs.pop("callback", None)

    losses = []
    mag_grads = []
    if record_file:
        # Open up the data file and write the column headers.
        with open(record_file, "w") as f:
            f.write("loss,grad_loss\n")

    # Define a callback function that appends to this data file.
    def minimize_callback(xk, state=None):
        # Record the loss and loss gradient.
        loss = Jhat.functional.block_variable.checkpoint
        grad_loss = [c.get_derivative() for c in Jhat.controls]
        grad_loss = sum(onp.sqrt(coo(g)._ad_dot(g)) for g in grad_loss)

        losses.append(loss)
        mag_grads.append(grad_loss)
        if record_file is not None:
            with open(record_file, "a") as f:
                f.write("%g,%g\n" % (loss, grad_loss))
        if write_C:
            with open("params.txt", "a") as f:
                L_vec = Jhat.controls[0].tape_value().arr
                C = get_C_from_vec(L_vec)
                f.write("param: %s\n" % str(C))

        if old_callback is not None:
            return old_callback(xk, state=state)

    params = minimize(Jhat, callback=minimize_callback, **kwargs)
    return params

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    utils.add_bool_arg(parser, 'debug', False, "show Jacobian singular values")
    parser.add_argument("--seed", type=int, help="seed for RNG", default=None)
    parser.add_argument("-T", "--T", type=float, help="Simulation time", default=1)
    parser.add_argument("--Nt", type=int, help="Number of time increments", default=10)

    modes = ['jax', 'network', 'energy']
    parser.add_argument('mode', choices=modes, help='specify which CR type to use', nargs="?", default=None)

    stiff_mat_type_info = (('cholesky', '`C = L @ L.T`'),
                          ('exponential', '`C = exp(L + L.T)`'))
    stiff_mat_type_help = 'Controls how C is generated from L.\n'
    stiff_mat_type_help += '\n'.join(f"- `{mode}`: {desc}" for mode, desc in stiff_mat_type_info)
    parser.add_argument("--stiff_mat_type", type=str, choices=[m for m,d in stiff_mat_type_info], help=stiff_mat_type_help, default='cholesky')

    stiff_mode_info = (('anisotropic', 'L is lower triangular.'),
             ('orthotropic', 'L has a lower triangular `dim x dim` block and the rest is diagonal.'),
             ('full', 'L is dense'))
    stiff_mode_help = 'Controls how L is generated from the network.\n'
    stiff_mode_help += '\n'.join(f"- `{mode}`: {desc}"for mode, desc in stiff_mode_info)
    parser.add_argument("--stiff_mode", type=str, choices=[m for m,d in stiff_mode_info], help=stiff_mode_help, default='orthotropic')
    utils.add_bool_arg(parser, 'stiff_chol_exponential', False, "Use `C = exp(L @ L.T)` instead of `C = L @ L.T`. This ensures positive definiteness.")
    utils.add_bool_arg(parser, "viz_tape", False, "Visualize Pyadjoint tape with TensorBoard")

    utils.add_bool_arg(
        parser,
        "show",
        True,
        "Don't show plots interactively. This is useful when combined with `--save_dir`.",
    )

    parser.add_argument(
        "--save_dir",
        type=str,
        help="Directory to save figures to. By default, the figures won't be saved.",
        default="",
    )

    parser.add_argument(
        "--format",
        type=str,
        help="Image format to use for saving figures.",
        default="png",
    )

    parser.add_argument(
        "--quad",
        type=int,
        help="Quadrature degree used to assemble PDE weak form.",
        default=6,
    )

    utils.add_bool_arg(
        parser,
        "verify",
        False,
        "Do a Taylor test gradient verification, which can take a lot of time.",
    )

    parser.add_argument("--save_params_linear", type=str, help="File to write params after doing linear training.", default='params_linear.npy')
    parser.add_argument("--save_params_nonlinear", type=str, help="File to write params after doing nonlinear training.", default='params_nonlinear.npy')
    parser.add_argument("--load_params_linear", type=str, help="File to load params from before doing linear training.", default='')
    utils.add_bool_arg(parser, "skip_linear", False, "Skip the linear-regime experiments.")
    utils.add_bool_arg(parser, "skip_nonlinear", False, "Skip the nonlinear-regime experiments.")
    utils.add_bool_arg(parser, "train_linear", True, "Skip the linear-regime training.")
    utils.add_bool_arg(parser, "train_nonlinear", True, "Skip the nonlinear-regime training.")

    args = parser.parse_args()

    onp.random.seed(args.seed)

    if not args.debug:
        set_log_level(crikit.logging.WARNING)

    if args.save_dir != "":
        utils.FIGURE_SAVE_DIR = args.save_dir
        try:
            plt.rc("savefig", format=args.format)
        except Exception as e:
            print("Warning: couldn't set output image format:", e)
    
    # Create experiment and true CR.
    ex_kwargs = dict(quad_degree=args.quad)
    ex, true_cr, loading, plot_results = make_plate_experiment(args, ex_kwargs)

    # Choose observer and loss function.
    observer = lambda x: x
    loss = integral_loss

    def build_experiments(loading_params):
        p1, p2, p3 = loading_params

        # Set up boundary conditions.
        def left_boundary(x, on_boundary):
            return on_boundary and near(x[0], 0, 1e-5)

        def bottom_boundary(x, on_boundary):
            return on_boundary and near(x[1], 0, 1e-5)

        def right_boundary(x, on_boundary):
            return on_boundary and near(x[0], 1, 1e-5)

        def top_boundary(x, on_boundary):
            return on_boundary and near(x[1], 1, 1e-5)

        A_dir_bcs = [DirichletBC(ex.V, (0,0), bottom_boundary)]
        B_dir_bcs = [DirichletBC(ex.V, (0,0), left_boundary)]
        C_dir_bcs = B_dir_bcs

        class TopBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return top_boundary(x, on_boundary)
        class RightBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return right_boundary(x, on_boundary)
        class BottomBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return bottom_boundary(x, on_boundary)

        markers = MeshFunction("size_t", ex.mesh, ex.mesh.topology().dim() - 1, 0)
        TopBoundary().mark(markers, 1)
        RightBoundary().mark(markers, 2)
        BottomBoundary().mark(markers, 3)
        dtop = ds(1, domain=ex.mesh, subdomain_data=markers)
        dright = ds(2, domain=ex.mesh, subdomain_data=markers)
        dbottom = ds(3, domain=ex.mesh, subdomain_data=markers)

        A_rob_bcs = RobinBC(0, 1, loading, dtop)
        B_rob_bcs = RobinBC(0, 1, loading, dright)

        C_loading = Expression(("0", "py/(sqrt(2*pi)*sigma)*exp(-(x[0]-x0)*(x[0]-x0)/(sigma*sigma) ) * sin(t*pi)"), t=0, px=0, py=1, sigma=0.2, x0=5/6, degree=4)
        C_rob_bcs = RobinBC(0, 1, C_loading, dbottom)

        class ExperimentalSetup:
            def __init__(self, name, loads, bcs):
                self.name = name
                self.loads = loads
                self.bcs = bcs

        A_loads = [
                (0, p1),
                (0, -p1),
                (p3, 0),
                (-p3, 0),
                (p3/onp.sqrt(2), p1/onp.sqrt(2)),
                (0.75*p3, 0),
            ]
        B_loads = [
                (p1, 0),
                (-p1, 0),
                (0, p2),
                (0, -p2),
                (p1/onp.sqrt(2), p2/onp.sqrt(2)),
                (0, 0.75*p2),
            ]
        C_loads = [
                (0, p2)
            ]

        A = ExperimentalSetup('A', A_loads, (A_dir_bcs, A_rob_bcs))
        B = ExperimentalSetup('B', B_loads, (B_dir_bcs, B_rob_bcs))
        C = ExperimentalSetup('C', C_loads, (C_dir_bcs, C_rob_bcs))

        def run_training_experiments(cr, title='', **ex_kwargs):
            ys = []
            for setup in [A, B]:
                for i, p in enumerate(setup.loads[:-1]):
                    print(f'Running experiment {setup.name}{i+1} with loading {p}')
                    cr.reset_hidden_state()

                    # Set experiment loading.
                    loading.px, loading.py = p
                    ex.set_bcs(*setup.bcs)

                    # Run experiment.
                    us = ex.run(cr, **ex_kwargs)

                    plt.figure()
                    plot_results()
                    plt.title(f"{title} ({setup.name}{i})")
                    utils.saveplot()

                    ys += [observer(u) for u in us]

            return ys


        def run_test_experiments(cr, title='', **ex_kwargs):
            ys = []
            for setup in [A, B]:
                i = len(setup.loads) - 1
                p = setup.loads[i]
                print(f'Running experiment {setup.name}{i+1} with loading {p}')
                cr.reset_hidden_state()

                # Set experiment loading.
                loading.px, loading.py = p
                ex.set_bcs(*setup.bcs)

                # Run experiment.
                us = ex.run(cr, **ex_kwargs)

                plt.figure()
                plot_results()
                plt.title(f"{title} ({setup.name}{i+1})")
                utils.saveplot()

                ys += [observer(u) for u in us]

            old_pre_cback = ex.pre_cback
            def pre_cback(i, t):
                print(f'  At time {i}: {t}')
                C_loading.t = t
            ex.set_temporal_parameters(ex.load_steps, pre_cback, ex.post_cback)

            for setup in [C]:
                i = len(setup.loads) - 1
                p = setup.loads[i]
                print(f'Running experiment {setup.name}{i+1} with loading {p}')
                cr.reset_hidden_state()

                # Set experiment loading.
                loading.px, loading.py = p
                ex.set_bcs(*setup.bcs)

                # Run experiment.
                us = ex.run(cr, **ex_kwargs)

                plt.figure()
                plot_results()
                plt.title(f"{title} ({setup.name}{i+1})")
                utils.saveplot()

                ys += [observer(u) for u in us]

            ex.set_temporal_parameters(ex.load_steps, old_pre_cback, ex.post_cback)
            return ys
        return run_training_experiments, run_test_experiments


    if not args.skip_linear:
        # Generate observations for true CR.
        experiment_params_linear = (0.16, 0.016, 0.06)
        run_training_experiments_linear, run_testing_experiments_linear = build_experiments(experiment_params_linear)
        with stop_annotating():
            print('Generating linear-regime ground-truth with a UFL CR')
            y_trues_train = run_training_experiments_linear(true_cr, title='Ground-truth (elastic)', ufl=True, disp=args.debug)
            y_trues_test = run_testing_experiments_linear(true_cr, title='Ground-truth (elastic)', ufl=True, disp=args.debug)

    if args.mode is None:
        if args.show:
            plt.show()
        else:
            plt.close('all')
        return

    # Create candidate CR.
    if args.mode == 'jax':
        E = coo(jnp.array(100.))
        nu = coo(jnp.array(0.35))
        yield_stress = coo(jnp.array(0.97))
        H = coo(jnp.array(10.))

        E.tf_name = 'E'
        nu.tf_name = 'nu'
        yield_stress.tf_name = 'yield_stress'
        H.tf_name = 'H'

        cr = PlasticityCR(E, nu, yield_stress, hardening_modulus=H)
        params = [E, nu]
        bounds = ([jnp.array(1.), jnp.array(0.)], [jnp.array(1000.), jnp.array(0.5)])
        cr.set_params(params, bounds)
        cr.h = (jnp.array(50e0), jnp.array(1e-1))
    elif args.mode == 'network':
        cr = make_plastic_stiffness_network_cr(ex, args.seed, mat_type=args.stiff_mat_type, mode=args.stiff_mode, chol_exponential=args.stiff_chol_exponential)
    elif args.mode == 'energy':
        cr = make_plastic_energy_cr(ex, args.seed)
    else:
        raise NotImplementedError("Internal error: invalid mode  '%s'" % args.mode)

    if args.load_params_linear != '':
        params_array = onp.load(args.load_params_linear, allow_pickle=False)
        cr.set_params_numpy(params_array)
    print()

    print('     True CR stiffness tensor:')
    print(jnp.array([[float(true_cr.C[i,j]) for j in range(true_cr.C.ufl_shape[1])] for i in range(true_cr.C.ufl_shape[0])]))

    print('Candidate CR stiffness tensor:')
    print(get_C_from_vec(cr.params[0].arr))

    optimize_options = dict(disp=True, gtol=1e-10, ftol=1e-10)
    epsilons = [1e-2 / 2**i for i in range(12)]
    def linear_stuff():
        print()
        print('Running candidate CR in linear regime')
        y_preds_train = run_training_experiments_linear(cr, title='Candidate (elastic)', ufl=False, disp=args.debug)

        losses = [loss(y_true, y_pred) for y_true, y_pred in zip(y_trues_train, y_preds_train)]
        print(losses)
        J = sum(losses)

        print()
        print('Initial linear-regime loss: %g' % J)

        Jhat = ReducedFunctional(J, cr.params_controls)

        if args.viz_tape:
            get_working_tape().visualise(launch_tensorboard=True, open_in_browser=True)

        if args.verify:
            # Verify gradient is correct with a Taylor test.
            print()
            taylor_results = taylor_to_dict(Jhat, cr.params, cr.h, epsilons=epsilons, Hm=0)
            print_taylor_results(taylor_results)

        if args.train_linear:
            # Train CR to match observations.
            trained_params = optimize_params(Jhat, bounds=cr.params_bounds, options=optimize_options, record_file="loss_linear.csv", write_C=True)

            if args.verify:
                # Verify gradient is correct with a Taylor test.
                print()
                taylor_results = taylor_to_dict(Jhat, trained_params, cr.h, epsilons=epsilons, Hm=0)
                print_taylor_results(taylor_results)

            # Recompute predicted value and loss with the trained params.
            trained_J = Jhat(trained_params)
            trained_y_pred = Control(y_preds_train[0]).tape_value()
            print()
            print('Final linear-regime loss: %g' % trained_J)

            plt.figure()
            f = plot(trained_y_pred, mode="displacement")
            plt.colorbar(f)
            plt.title('Experiment output for trained CR (linear regime)')
            utils.saveplot()

            plt.figure()
            f = plot(y_trues_train[0] - trained_y_pred, mode="displacement")
            plt.colorbar(f)
            plt.title('Displacement error (u_true - u_trained) (linear regime)')
            utils.saveplot()

            cr.set_params(trained_params, bounds=cr.params_bounds)

            if args.save_params_linear != '':
                params_array = cr.get_params_numpy()
                onp.save(args.save_params_linear, params_array, allow_pickle=False)

            print()
            print('Candidate CR trained stiffness tensor:')
            print(get_C_from_vec(cr.params[0].arr))

        y_preds_test = run_testing_experiments_linear(cr, title='Candidate (elastic)', ufl=False, disp=args.debug)
        J = sum(loss(y_true, y_pred) for y_true, y_pred in zip(y_trues_test, y_preds_test))
        print()
        print('Testing error for linear-regime: %g' % J)

    if not args.skip_linear:
        with push_tape():
            linear_stuff()

    if args.show:
        plt.show()
    else:
        plt.close('all')

    if not args.skip_nonlinear:
        # Generate observations for true CR.
        experiment_params_nonlinear = (0.8, 0.08, 0.3)
        run_training_experiments_nonlinear, run_testing_experiments_nonlinear = build_experiments(experiment_params_nonlinear)
        with stop_annotating():
            print()
            print('Generating nonlinear-regime ground-truth with a UFL CR')
            y_trues_train = run_training_experiments_nonlinear(true_cr, title='Ground-truth (plastic)', ufl=True, disp=args.debug)
            y_trues_test = run_testing_experiments_nonlinear(true_cr, title='Ground-truth (plastic)', ufl=True, disp=args.debug)

        with push_tape():
            print()
            print('Running candidate CR in nonlinear-regime')
            y_preds_train = run_training_experiments_nonlinear(cr, title='Candidate (plastic)', ufl=False, disp=args.debug)

            losses = [loss(y_true, y_pred) for y_true, y_pred in zip(y_trues_train, y_preds_train)]
            print(losses)
            J = sum(losses)

            print()
            print('Initial nonlinear-regime loss: %g' % J)

            nonlinear_params = cr.params[1:]
            nonlinear_params_controls = cr.params_controls[1:]
            nonlinear_h = cr.h[1:]
            if cr.params_bounds is not None:
                nonlinear_params_bounds = (cr.params_bounds[0][1:], cr.params_bounds[1][1:])
            else:
                nonlinear_params_bounds = None
            Jhat = ReducedFunctional(J, nonlinear_params_controls)

            if args.viz_tape:
                get_working_tape().visualise(launch_tensorboard=True, open_in_browser=True)

            if args.verify:
                # Verify gradient is correct with a Taylor test.
                print()
                taylor_results = taylor_to_dict(Jhat, nonlinear_params, nonlinear_h, epsilons=epsilons, Hm=0)
                print_taylor_results(taylor_results)

            if args.train_nonlinear:
                # Train CR to match observations.
                trained_nonlinear_params = optimize_params(Jhat, bounds=nonlinear_params_bounds, options=optimize_options, record_file="loss_nonlinear.csv")

                if args.verify:
                    # Verify gradient is correct with a Taylor test.
                    print()
                    taylor_results = taylor_to_dict(Jhat, trained_nonlinear_params, nonlinear_h, epsilons=epsilons, Hm=0)
                    print_taylor_results(taylor_results)

                # Recompute predicted value and loss with the trained params.
                trained_J = Jhat(trained_nonlinear_params)
                trained_y_pred = Control(y_preds_train[0]).tape_value()
                print()
                print('Final nonlinear-regime loss: %g' % trained_J)

                plt.figure()
                f = plot(trained_y_pred, mode="displacement")
                plt.colorbar(f)
                plt.title('Experiment output for trained CR')
                utils.saveplot()

                plt.figure()
                f = plot(y_trues_train[0] - trained_y_pred, mode="displacement")
                plt.colorbar(f)
                plt.title('Displacement error (u_true - u_trained)')
                utils.saveplot()

                trained_params = cr.params[0:1] + trained_nonlinear_params
                cr.set_params(trained_params, bounds=cr.params_bounds)
                if args.save_params_nonlinear != '':
                    params_array = cr.get_params_numpy()
                    onp.save(args.save_params_nonlinear, params_array, allow_pickle=False)

            y_preds_test = run_testing_experiments_nonlinear(cr, title='Candidate (elastic)', ufl=False, disp=args.debug)
            J = sum(loss(y_true, y_pred) for y_true, y_pred in zip(y_trues_test, y_preds_test))
            print()
            print('Testing error for nonlinear-regime: %g' % J)

    if args.show:
        plt.show()
    else:
        plt.close('all')

if __name__ == '__main__':
    main()
