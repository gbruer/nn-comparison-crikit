import tensorflow

from crikit import *
from pyadjoint.overloaded_type import create_overloaded_object as coo

import argparse
import crikit.utils as utils
import jax
import jax.numpy as jnp
import matplotlib.pyplot as plt
import numpy as onp

from cr import ParamsCR, ParamsCRUFL, SPDIntegratorCR
from experiment import EquilibriumExperiment, RobinBC
from network_builders import FICNN, FIQCNN, MLP, PositiveMonotonicNN
from utils import print_taylor_results

import logging
ffc_logger = logging.getLogger('FFC')
ffc_logger.setLevel(logging.WARNING)

def create_ufl_standin(shape, **kwargs):
    return create_ufl_standins((shape,))[0]

def make_ufl_cr_plap(ex):
    def elastic_stress(strain, p, eps2):
        e2 = tr(dot(strain, strain))
        mu = (e2 + eps2)**((p-2)/2)
        return mu * strain

    eps2 = Constant(1e-10)
    p = Constant(3)
    strain = create_ufl_standin((ex.dim,)*2)
    output = elastic_stress(strain, p, eps2)
    cr = ParamsCRUFL(UFLExprSpace(strain), output, {strain: 0})

    cr.set_params((p, eps2), bounds=((1,None), (0, None)))
    cr.h = (Constant(1e-1), Constant(1e-11))
    return cr

def make_ufl_cr_rivlin(ex):
    def elastic_stress(C, c1, c2):
        I = Identity(2)
        # C = 2*strain + I
        C_33 = 1/det(C)
        I1 = tr(C) + C_33

        S_iso = 2*c1*I + 2*c2*(C + I1*I)
        S_33 = 2*c1 + 2*c2*(C_33 + I1)

        p = -C_33 * S_33
        C_inv = inv(C)
        S = S_iso + p * C_inv
        return S

    c1 = Constant(0.1863)
    c2 = Constant(0.0097)
    strain = create_ufl_standin((ex.dim,)*2)
    output = elastic_stress(strain, c1, c2)
    cr = ParamsCRUFL(UFLExprSpace(strain), output, {strain: 0})

    cr.set_params((c1, c2), bounds=((0, None), (0, None)))
    cr.h = (Constant(1e-1), Constant(1e-2))
    return cr

def make_direct_network_cr(ex, seed):
    # Create a dummy CR to see how many inputs and outputs the network needs.
    cr = ParamsCR(ex.cr_output_type, ex.cr_input_types, strain_energy=False)
    num_inputs = cr.cr_input_shape[0]
    num_outputs = cr.num_scalar_functions

    # Create the network and the corresponding CR.
    network = PositiveMonotonicNN(num_outputs, layer_sizes=[5])
    network_eval, overloaded_flat_params, bounds = initialize_network(network, seed, num_inputs)

    cr = ParamsCR(ex.cr_output_type, ex.cr_input_types, network_eval, nojit=False, params=overloaded_flat_params, strain_energy=False)
    cr.set_params(overloaded_flat_params, bounds=bounds)
    return cr

def make_energy_network_cr(ex, seed, vmap=True):
    # Create a dummy CR to see how many inputs and outputs the network needs.
    output_type = TensorType.make_scalar()
    num_inputs, num_outputs = cr_function_shape(output_type, ex.cr_input_types)

    # Create the network and the corresponding CR.
    # network = FICNN(num_outputs, layer_sizes=[5])
    network = FIQCNN(num_outputs, convex_outputs=5, convex_layer_sizes=[5,5], monotonic_layer_sizes=[5,5])
    network_eval, overloaded_flat_params, bounds = initialize_network(network, seed, num_inputs)

    cr = ParamsCR(output_type, ex.cr_input_types, network_eval, nojit=False, params=overloaded_flat_params, strain_energy=True, vmap=vmap)
    cr.set_params(overloaded_flat_params, bounds=bounds)
    return cr

def make_stiffness_network(num_inputs, dim, seed, mat_type='cholesky', mode='orthotropic', chol_exponential=False):
    strain_vec_dim = ((dim+1) * dim) // 2

    if mat_type == 'exponential' or chol_exponential:
        output_activation = 'linear'
    else:
        output_activation = 'softplus'

    if mode == 'anisotropic':
        tril_indices = jnp.tril_indices(strain_vec_dim)
        num_outputs = tril_indices[0].size
    elif mode == 'orthotropic':
        tril_indices = jnp.tril_indices(dim)
        first_block_outputs = tril_indices[0].size
        second_block_outputs = strain_vec_dim - dim
        num_outputs = first_block_outputs + second_block_outputs
    elif mode == 'full':
        num_outputs = strain_vec_dim**2
    else:
        raise ValueError('Invalid mode: %s' % mode)

    # Create the network and the corresponding CR.
    network = MLP(num_outputs, output_activation, layer_sizes=[20, 20, 20])
    network_eval, overloaded_flat_params, bounds = initialize_network(network, seed, num_inputs)

    def G_func(inputs, *params):
        L_vec = network_eval(inputs, *params)

        if mode in 'anisotropic':
            L = jnp.zeros((strain_vec_dim, strain_vec_dim))
            L = jax.ops.index_update(L, tril_indices, L_vec)
        elif mode == 'orthotropic':
            L = jnp.zeros((strain_vec_dim, strain_vec_dim))
            L1 = jnp.zeros((dim, dim))
            L1 = jax.ops.index_update(L1, tril_indices, L_vec[:first_block_outputs])
            L2 = jnp.diag(L_vec[first_block_outputs:])
            L = jax.scipy.linalg.block_diag(L1, L2)
        elif mode == 'full':
            L = jnp.reshape(L_vec, (strain_vec_dim, strain_vec_dim))
        else:
            raise ValueError('Invalid mode: %s' % mode)
        if mat_type == 'cholesky':
            G = L @ L.T
            if chol_exponential:
                G = jax.scipy.linalg.expm(G)
        elif mat_type == 'exponential':
            G = L + L.T
            G = jax.scipy.linalg.expm(G)
        return G
    return G_func, overloaded_flat_params, bounds

def make_stiffness_network_cr(ex, seed, **kwargs):
    num_inputs,_  = cr_function_shape(ex.cr_output_type, ex.cr_input_types)
    G_func, overloaded_flat_params, bounds = make_stiffness_network(num_inputs, ex.dim, seed, **kwargs)
    cr = SPDIntegratorCR(ex.cr_output_type, ex.cr_input_types, G_func, integration_mode='none',
                         nojit=False, params=overloaded_flat_params, strain_energy=False)
    cr.set_params(overloaded_flat_params, bounds=bounds)
    return cr

def initialize_network(network, seed, num_inputs):
    # Initialize weights by giving dummy input.
    key1, key2 = jax.random.split(jax.random.PRNGKey(0 if seed is None else seed))
    x = jax.random.normal(key1, (num_inputs,))
    params = network.init(key2, x)

    # Print out network information
    network_shapes = jax.tree_map(lambda p: p.shape, params)
    print('Network info:', network_shapes)

    # Make CR function that acts on the flattened param tree.
    flat_params, tree_info = jax.tree_util.tree_flatten(params)
    overloaded_flat_params = [coo(p) for p in flat_params]
    def network_eval(inputs, *flat_params):
        params = jax.tree_util.tree_unflatten(tree_info, flat_params)
        return network.apply(params, inputs)

    # Set bounds (just to enforce nonnegativity on certain params).
    nonnegatives = []
    for key in params['params']:
        if key.startswith('nonnegative_'):
            nonnegatives.append(params['params'][key]['kernel'])
    if len(nonnegatives) == 0:
        bounds = None
    else:
        lower_bounds = [0 if any(p is nn for nn in nonnegatives) else -onp.inf for p in flat_params]
        upper_bounds = [onp.inf] * len(flat_params)
        bounds = [lower_bounds, upper_bounds]

    return network_eval, overloaded_flat_params, bounds


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    utils.add_bool_arg(parser, 'show', True, "don't show plots interactively")
    utils.add_bool_arg(parser, 'debug', False, "show Jacobian singular values")
    parser.add_argument("--seed", type=int, help="seed for RNG", default=None)
    modes = ['direct', 'energy', 'stiffness']
    parser.add_argument('mode', choices=modes, help='specify which network type to use', nargs="?", default=None)

    true_cr_info = (('rivlin', 'An incompressible Rivilin-Saunders material energy `c1*(I1-1) + c2*(I2-1)`'),
                    ('plap', 'p-Laplacian model'))
    true_cr_help = 'Sets the ground-truth CR.\n\n'
    true_cr_help += '\n'.join(f"- `{mode}`: {desc}" for mode, desc in true_cr_info)
    parser.add_argument("--true_cr", type=str, choices=[m for m,d in true_cr_info], help=true_cr_help, default='rivlin')

    stiff_mat_type_info = (('cholesky', '`C = L @ L.T`'),
                          ('exponential', '`C = exp(L + L.T)`'))
    stiff_mat_type_help = 'Controls how C is generated from L.\n'
    stiff_mat_type_help += '\n'.join(f"- `{mode}`: {desc}" for mode, desc in stiff_mat_type_info)
    parser.add_argument("--stiff_mat_type", type=str, choices=[m for m,d in stiff_mat_type_info], help=stiff_mat_type_help, default='cholesky')

    stiff_mode_info = (('anisotropic', 'L is lower triangular.'),
             ('orthotropic', 'L has a lower triangular `dim x dim` block and the rest is diagonal.'),
             ('full', 'L is dense'))
    stiff_mode_help = 'Controls how L is generated from the network.\n'
    stiff_mode_help += '\n'.join(f"- `{mode}`: {desc}"for mode, desc in stiff_mode_info)
    parser.add_argument("--stiff_mode", type=str, choices=[m for m,d in stiff_mode_info], help=stiff_mode_help, default='orthotropic')
    utils.add_bool_arg(parser, 'stiff_chol_exponential', False, "Use `C = exp(L @ L.T)` instead of `C = L @ L.T`. This ensures positive definiteness.")

    args = parser.parse_args()

    onp.random.seed(args.seed)
    tape = get_working_tape()

    # Create experiment and true CR.
    mesh = UnitSquareMesh(10, 5)
    ex = EquilibriumExperiment(mesh, quad_degree=10, g=0.05)
    if args.true_cr == 'rivlin':
        true_cr = make_ufl_cr_rivlin(ex)
    elif args.true_cr == 'plap':
        true_cr = make_ufl_cr_plap(ex)
    else:
        raise NotImplementedError("Internal error: invalid true CR type  '%s'" % args.true)


    # Set up boundary conditions.
    left = CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)
    right = CompiledSubDomain("near(x[0], side) && on_boundary", side = 1.0)
    fixed = Constant((0, 0), name='dirichlet_bc')


    markers = MeshFunction("size_t", ex.mesh, ex.mesh.topology().dim() - 1, 0)
    right.mark(markers, 1)
    dright = ds(1, domain=ex.mesh, subdomain_data=markers)
    squeezed = Expression(("0.2", "scale*sin(y0 - x[1])"), scale=0.1, y0=0.5, degree=6)
    rob_bcs = RobinBC(0, 1, squeezed, dright)

    dir_bcs = [DirichletBC(ex.V, fixed, left), DirichletBC(ex.V, squeezed, right)]
    ex.set_bcs(dir_bcs, rob_bcs=rob_bcs)

    # Choose observer and loss function.
    observer = lambda x: x
    loss = integral_loss

    # Generate observations for true CR. This computation does not need to be annotated because the true CR is fixed.
    with stop_annotating():
        y_true = observer(ex.run(true_cr, ufl=True))

    # Plot solution.
    plt.figure()
    f = plot(y_true, mode="displacement")
    plt.colorbar(f)
    plt.title('True experiment output')
    if args.mode is None:
        if args.show:
            plt.show()
        return

    # Create candidate CR.
    if args.mode == 'direct':
        cr = make_direct_network_cr(ex, args.seed)
    elif args.mode == 'energy':
        cr = make_energy_network_cr(ex, args.seed)
    elif args.mode == 'stiffness':
        cr = make_stiffness_network_cr(ex, args.seed, mat_type=args.stiff_mat_type, mode=args.stiff_mode, chol_exponential=args.stiff_chol_exponential)
    else:
        raise NotImplementedError("Internal error: invalid mode  '%s'" % args.mode)

    # Run experiment with candidate CR and compute loss.
    with tape.name_scope('Initial-Experiment'):
        y_pred = observer(ex.run(cr, ufl=False, disp=args.debug))
    J = loss(y_true, y_pred)
    Jhat = ReducedFunctional(J, cr.params_controls)

    print()
    print('Initial loss: %g' % J)

    # Plot solution.
    plt.figure()
    f = plot(y_pred, mode="displacement")
    plt.colorbar(f)
    plt.title('Experiment output for candidate CR')
    if args.show:
        plt.show()

    # Verify gradient is correct with a Taylor test.
    print()
    epsilons = [1.0 / 2**i for i in range(15)]
    taylor_results = taylor_to_dict(Jhat, cr.params, cr.h, epsilons=epsilons, Hm=0)
    print_taylor_results(taylor_results)


    # Train CR to match observations.
    trained_params = minimize(Jhat, bounds=cr.params_bounds, options={'disp':True})

    # Verify gradient is correct with a Taylor test.
    print()
    epsilons = [1.0 / 2**i for i in range(15)]
    taylor_results = taylor_to_dict(Jhat, trained_params, cr.h, epsilons=epsilons, Hm=0)
    print_taylor_results(taylor_results)

    # Recompute predicted value and loss with the trained params.
    trained_J = Jhat(trained_params)
    trained_y_pred = Control(y_pred).tape_value()
    print()
    print('Final loss: %g' % trained_J)

    plt.figure()
    f = plot(trained_y_pred, mode="displacement")
    plt.colorbar(f)
    plt.title('Experiment output for trained CR')


    plt.figure()
    f = plot(y_true - trained_y_pred, mode="displacement")
    plt.colorbar(f)
    plt.title('Displacement error (u_true - u_trained)')

    if args.show:
        plt.show()


if __name__ == '__main__':
    main()