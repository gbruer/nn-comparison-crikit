from crikit import *
from crikit.cr.quadrature import make_quadrature_space
from crikit.cr.space_builders import DirectSum
import numpy as onp
import ufl

class RobinBC:
    r"""Represents boundary conditions of the form :math:`a u + b \sigma n = j` on a boundary ds,
       where :math:`a,b` are scalars, :math:`n, j` are vectors, with `n` being the normal vector.

    The relevant term in the weak form is :math:`\int v \cdot \sigma n \, ds`. The boundary
    condition is applied by replacing that term with `inner(v, (a*u - j))/b * ds`.

    Note: this class can represent Neumann conditions by setting a to 0.
    """
    def __init__(self, a, b, j, ds):
        self.a = a
        self.b = b
        self.j = j
        self.ds = ds

def get_space_ufl_standins(space):
    if isinstance(space, DirectSum):
        return [get_space_ufl_standins(s) for s in space]
    shape = tuple(i for i in space.shape() if i != -1)
    return create_ufl_standins((shape,))[0]

class Experiment:
    def set_bcs(self, dir_bcs, rob_bcs=None):
        """
        Sets Dirichlet and Robin boundary conditions. Each Robin condition is a
        tuple (a, b, j, ds) corresponding to the equation :math:`a u + b stress n = j`
        on the boundary ds. Note that the Robin conditions are only applied
        to the velocity, not the pressure.

        Args:
            dir_bcs (DirichletBC or list[DirichletBC]): The Dirichlet boundary conditions.
            rob_bcs (RobinBC or list[RobinBC]): See description above.
        """
        self.bcs = Enlist(dir_bcs)
        self.h_bcs = homogenize_bcs(self.bcs)
        self.rob_bcs = Enlist(rob_bcs) if rob_bcs is not None else None

    def bc(self, u):
        """Applies Dirichlet boundary conditions to function or to matrix"""
        bc_u = u.vector() if hasattr(u, 'vector') else u
        for bc in self.bcs:
            bc.apply(bc_u)
        return u

    def h_bc(self, u):
        """Applies homogenous Dirichlet boundary conditions to function or to matrix"""
        bc_u = u.vector() if hasattr(u, 'vector') else u
        for bc in self.h_bcs:
            bc.apply(bc_u)
        return u

    def get_robin_terms(self, u, v):
        """Adds the Robin terms to the given Form"""
        if self.rob_bcs is None:
            return 0
        F = 0
        for r in self.rob_bcs:
            F += inner(v, (r.a * u - r.j))/r.b * r.ds
        return F

    def run(self, cr, observer=None, initial=None, ufl=False, cback=None, solver_parameters=None, disp=True):
        if solver_parameters is None:
            solver_parameters = {}

        # Define u.
        if initial is None:
            u = Function(self.V, name='u')
        else:
            u = initial

        if ufl:
            u = self._ufl_solve(cr, u, cback=cback, disp=disp, solver_parameters=solver_parameters)
        else:
            u = self._solve(cr, u, cback=cback, disp=disp)
        if observer is None:
            return u
        return observer(u)

    def _ufl_solve(self, cr, u, cback=None, disp=True, extra_form_inputs=(), cr_inputs=None, solver_parameters=None):
        # Don't print out Newton iterations.
        if not disp:
            orig_log_level = get_log_level()
            set_log_level(int(LogLevel.CRITICAL))

        # Set up the weak form.
        cr_inputs = self.get_cr_inputs(u) if cr_inputs is None else cr_inputs
        cr_outputs = cr(cr_inputs)
        F = self.get_form(u, cr_outputs, extra_form_inputs)

        solver_parameters = dict(newton_solver=dict(maximum_iterations=100))
        solve(F == 0, u, self.bcs, solver_parameters=solver_parameters)

        if not disp:
            set_log_level(orig_log_level)

        if cback is not None:
            cback(u)
        return u

    def _solve(self, cr, u, cback=None, disp=True, extra_form_inputs=(), cr_inputs=None):
        # Set up the weak form.
        F, cr_output_standins = self.get_form_cr(cr, u, extra_form_inputs)
        cr_inputs = self.get_cr_inputs(u) if cr_inputs is None else cr_inputs

        with push_tape():
            residual = Function(self.V)
            assemble_with_cr(F, cr, cr_inputs, cr_output_standins, tensor=residual, quad_params=self.quad_params)

            ucontrol = Control(u)
            residual_rf = ReducedFunction(residual, ucontrol)
        reduced_equation = ReducedEquation(residual_rf, self.bc, self.h_bc)

        solver = SNESSolver(reduced_equation, {'jmat_type' : 'assembled'})
        u = solver.solve(ucontrol, disp=disp, cback=cback)
        return u

    def get_form_cr(self, cr, u, extra_form_inputs):
        cr_output_standins = get_space_ufl_standins(cr.target)
        F = self.get_form(u, cr_output_standins, extra_form_inputs)
        return F, cr_output_standins

    def get_cr_inputs(self, u):
        """ Must be implemented. This should express the inputs to the CR as UFL operations on u. E.g., sym(grad(u)).

        Args:
            u (Function): The solution variable for the PDE.

        Returns:
            ufl.core.Expr or list[ufl.core.Expr]: the inputs to the CR.
        """
        raise NotImplementedError


    def get_form(self, u, cr_outputs):
        """Must be implemented. This should return the a Form representing the PDE.

        Args:
            u (Function): The solution variable for the PDE.
            cr_outputs (ufl.core.Expr or tuple[ufl.core.Expr]): The outputs of the CR, possibly placeholder outputs.
        Returns:
            ufl.form.Form: the weak form the of the PDE.
        """
        raise NotImplementedError


class EquilibriumExperiment(Experiment):
    def __init__(self, mesh, quad_degree, alpha=0, g=9.8, rho=1):
        self.set_domain_parameters(mesh, quad_degree)
        self.set_f(alpha=alpha, g=g, rho=rho)

        self.cr_input_types = (TensorType.make_symmetric(2,self.dim),)
        self.cr_output_type = TensorType.make_symmetric(2,self.dim)

    def set_f(self, alpha=None, g=None, rho=None):
        self.alpha = self.alpha if alpha is None else alpha
        self.g = self.g if g is None else g
        self.rho = self.rho if rho is None else rho
        f = (-self.rho * self.g * sin(self.alpha), -self.rho * self.g * cos(self.alpha), 0)
        if self.dim == 2:
            f = f[:2]
        self.f = Constant(f, name='f')

    def set_domain_parameters(self, mesh, quad_degree):
        self.mesh = mesh
        self.dim = self.mesh.geometric_dimension()
        self.V = VectorFunctionSpace(self.mesh, "CG", 2)
        self.quad_params = {'quadrature_degree': quad_degree}
        set_default_covering_params(domain=self.mesh.ufl_domain(), quad_params=self.quad_params)

    def get_cr_inputs(self, u):
        F = Identity(self.dim) + grad(u)
        C = dot(F, F.T)
        return C

    def get_form(self, u, stress, extra):
        v = TestFunction(self.V)
        qdx = dx(metadata=dict(self.quad_params, representation='uflacs'))

        lhs = inner(stress, sym(grad(v))) * qdx
        rhs = inner(self.f, v) * qdx
        F = lhs - rhs
        F = F + self.get_robin_terms(u, v)
        return F

class PlasticityCRExperiment(Experiment):
    def __init__(self, mesh, quad_degree):
        self.set_domain_parameters(mesh, quad_degree)
        self.f = ufl.zero(self.dim)
        self.set_temporal_parameters((1,), lambda t: None, lambda i,t,u: None)

        strain = TensorType.make_symmetric(2,self.dim)
        strain_rate_or_previous_strain = TensorType.make_symmetric(2,self.dim)
        previous_stress = TensorType.make_symmetric(2,self.dim)
        self.cr_input_types = (strain, strain_rate_or_previous_strain, previous_stress)

        stress_or_stress_rate = TensorType.make_symmetric(2,self.dim)
        self.cr_output_type = stress_or_stress_rate


    def set_domain_parameters(self, mesh, quad_degree):
        self.quad_params = {'quadrature_degree': quad_degree}

        self.mesh = mesh
        self.QV = make_quadrature_space((3,), self.quad_params, domain=self.mesh.ufl_domain())
        self.Q = make_quadrature_space((), self.quad_params, domain=self.mesh.ufl_domain())

        self.dim = self.mesh.geometric_dimension()
        self.V = VectorFunctionSpace(self.mesh, "CG", 2)
        set_default_covering_params(domain=self.mesh.ufl_domain(), quad_params=self.quad_params)

    def get_form(self, Du, stress_vec, extra):
        old_u, old_stress = extra
        def get_tensor(v):
            return as_tensor([[v[0], v[2]], [v[2], v[1]]])
        v = TestFunction(self.V)

        stress = get_tensor(stress_vec)
        qdx = dx(metadata=dict(self.quad_params, representation='uflacs'))
        F = inner(stress, sym(grad(v))) * qdx
        if not isinstance(self.f, ufl.constantvalue.Zero):
            rhs = inner(self.f, v) * qdx
            F = F - rhs
        F = F + self.get_robin_terms(old_u + Du, v)
        return F

    def set_temporal_parameters(self, load_steps=None, pre_cback=None, post_cback=None):
        if load_steps is not None:
            self.load_steps = load_steps
        if pre_cback is not None:
            self.pre_cback = pre_cback
        if post_cback is not None:
            self.post_cback = post_cback

    def get_cr_inputs(self, u, Du, old_stress):
        def get_vector(t):
            return as_vector([t[0,0], t[1,1], t[0,1]])

        old_strain = sym(grad(u))
        strain = sym(grad(u + Du))
        cr_inputs = (get_vector(strain), get_vector(old_strain), old_stress)
        return cr_inputs

    def _backend_solve(self, cr, u, ufl=False, cback=None, disp=True):
        stress = Function(self.QV)
        old_stress = Function(self.QV)
        Du = Function(self.V, name="Current increment")

        if not ufl:
            cr_inputs = self.get_cr_inputs(u, Du, old_stress)
            ufl_input_space = DirectSum(tuple(UFLExprSpace(x) for x in cr_inputs))
            ufl_output_space = UFLFunctionSpace(self.QV)
            project_cr_inputs = get_composite_cr(ufl_input_space, cr.source)
            project_cr_outputs = get_composite_cr(cr.target, ufl_output_space)

        tape = get_working_tape()
        us = []
        for (i, t) in enumerate(self.load_steps):
            self.pre_cback(i, t)

            # Solve stuff here.
            # Du.interpolate(Constant((1e-15, 1e-15)))
            Du.vector()[:] = onp.random.uniform(-1e-11, 1e-11, Du.vector().size())

            extra_form_inputs = (u, old_stress)
            if not ufl:
                # Solve for Du.
                cr_inputs = self.get_cr_inputs(u, Du, old_stress)

                Du = self._solve(cr, Du, cback=cback, disp=disp, cr_inputs=cr_inputs,
                                 extra_form_inputs=extra_form_inputs)

                # Get current stress state.
                with tape.name_scope('Get-stress'):
                    cr_inputs = self.get_cr_inputs(u, Du, old_stress)
                    cr_inputs_array = project_cr_inputs(cr_inputs)
                    stress_array = cr.update_hidden_state(cr_inputs_array)
                    stress = project_cr_outputs(stress_array)
            else:
                # Solve for Du.
                cr_inputs = self.get_cr_inputs(u, Du, old_stress)

                parameters = dict(newton_solver=dict(absolute_tolerance=1e-8))
                Du = self._ufl_solve(cr, Du, cback=cback, disp=disp, cr_inputs=cr_inputs,
                                     extra_form_inputs=extra_form_inputs, solver_parameters=parameters)

                # Get current stress state.
                cr_inputs = self.get_cr_inputs(u, Du, old_stress)

                stress = cr.update_hidden_state(cr_inputs)
                stress = project(stress, self.QV)

            # Update state variables.
            u.assign(u+Du)
            old_stress.assign(stress)

            us.append(u.copy(deepcopy=True))
            self.post_cback(i, t, u)

        do_plots = False
        if do_plots:
            import matplotlib.pyplot as plt
            plt.figure()
            f = plot(project(as_vector([old_stress[0], old_stress[1]]), self.V))
            plt.title(f'Increment {i+1}: stress diag')
            plt.colorbar(f)

            plt.figure()
            f = plot(project(old_stress[2], FunctionSpace(self.mesh, "CG", 2)))
            plt.title(f'Increment {i+1}: stress off-diag')
            plt.colorbar(f)

            plt.figure()
            f = plot(Du)
            plt.title(f'Increment {i+1}: Du')
            plt.colorbar(f)

            plt.figure()
            f = plot(u)
            plt.title(f'Increment {i+1}: u')
            plt.colorbar(f)

            plt.figure()
            f = plot(u, mode='displacement')
            plt.colorbar(f)
            plt.title('u')

            plt.show()
        return us

    def run(self, cr, observer=None, initial=None, ufl=False, cback=None, disp=True):
        # Define u.
        u = Function(self.V, name='u') if initial is None else initial
        us = self._backend_solve(cr, u, ufl=ufl, cback=cback, disp=disp)
        return us if observer is None else [observer(u) for u in us]
