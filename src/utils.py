
def print_taylor_results(results, include_hessian=False):
    if include_hessian:
        table_info = [
                        ('eps', results["eps"]),
                        ('J(m + eps*h)', results["Jps"]),
                        ('Residuals', 3, True),
                        ('Zeroth order', results["R0"]["Residual"]),
                        ('First order', results["R1"]["Residual"]),
                        ('Second order', results["R2"]["Residual"]),
                        ('Convergence Rates', 3, True),
                        ('Zeroth order', results["R0"]["Rate"]),
                        ('First order', results["R1"]["Rate"]),
                        ('Second order', results["R2"]["Rate"]),
                     ]
    else:
        table_info = [
                        ('eps', results["eps"]),
                        ('J(m + eps*h)', results["Jps"]),
                        ('Residuals', 2, True),
                        ('Zeroth order', results["R0"]["Residual"]),
                        ('First order', results["R1"]["Residual"]),
                        ('Convergence Rates', 2, True),
                        ('Zeroth order', results["R0"]["Rate"]),
                        ('First order', results["R1"]["Rate"]),
                     ]

    column_width = 15

    N = column_width * len([v for v in table_info if len(v) == 2])
    print('='*N)
    print('Taylor test results')

    # Print top headers.
    skip = 0
    for i,v in enumerate(table_info):
        if skip > 0:
            skip -= 1
            continue
        if len(v) > 2:
            print(f'|{v[0]:^{column_width*v[1]-3}s}| ', end='')
            skip = v[1]
        else:
            print(f' {"":{column_width-1}s}', end='')
    print()

    # Print headers.
    for i,v in enumerate(table_info):
        if len(v) == 2:
            print(f' {v[0]:{column_width-1}s}', end='')
            n = len(v[1])
    print()

    # Print data.
    for j in range(n):
        for i,v in enumerate(table_info):
            if len(v) == 2:
                x = v[1][j]
                if isinstance(x, list) and len(x) == 1:
                    x = x[0]
                x = float(x)
                x_s = f'{x: {column_width-5}e}'
                print(f' {x_s:{column_width-1}s}', end='')
        print()

    print(f'   J(m): {float(results["Jm"]): e}')
    print(f'dJdm(m): {float(results["dJdm"]): e}')
    print('='*N)

