import jax.numpy as jnp
from jax import random
from flax import linen as nn
from typing import List, Tuple

from jax.config import config
config.enable_omnistaging() # Linen requires enabling omnistaging

def uniform(wmin, wmax, dtype=jnp.float64):
    scale = wmax - wmin
    def init(key, shape, dtype=dtype):
        return random.uniform(key, shape, dtype) * scale + wmin
    return init

def zeros(key, shape, dtype=jnp.float64):
    return jnp.zeros(shape, dtype)

class FICNN(nn.Module):
    """A Fully Input Convex Neural Network model.

    Based on "Input Convex Neural Networks" by Amos et al: https://arxiv.org/pdf/1609.07152.pdf
    """

    num_outputs: int
    layer_sizes: List[int]

    def setup(self):
        self.nonneg_init = uniform(0, 0.003)
        self.normal_init = uniform(-0.003, 0.003)

    @nn.compact
    def __call__(self, x):
        x = x.flatten()
        inputs = x
        x_hidden_layer_kwargs = {
            'dtype': jnp.float64,
            'bias_init': zeros,
            'kernel_init': self.nonneg_init,
        }
        y_hidden_layer_kwargs = {
            'dtype': jnp.float64,
            'kernel_init': self.normal_init,
            'use_bias': False,
        }

        nonneg_i = 0
        for num in self.layer_sizes:
            x = nn.Dense(num, **x_hidden_layer_kwargs, name=f'nonnegative_{nonneg_i}')(x); nonneg_i += 1
            y = nn.Dense(num, **y_hidden_layer_kwargs)(inputs)
            x = nn.softplus(x + y)

        x = nn.Dense(self.num_outputs, **x_hidden_layer_kwargs, name=f'nonnegative_{nonneg_i}')(x); nonneg_i += 1
        y = nn.Dense(self.num_outputs, **y_hidden_layer_kwargs)(inputs)

        outputs = nn.softplus(x + y)
        if self.num_outputs == 1:
            outputs = outputs.reshape((-1,))
        return outputs

class PositiveMonotonicNN(nn.Module):
    """Based on Lang, Bernhard. "Monotonic multi-layer perceptron networks as universal
        approximators." International conference on artificial neural networks. Springer, Berlin, Heidelberg, 2005."""

    num_outputs: int
    layer_sizes: List[int]

    def setup(self):
        self.nonneg_init = uniform(0, 0.003)
        self.nonneg_i = 0

    @nn.compact
    def __call__(self, x):
        hidden_layer_kwargs = {
            'dtype': jnp.float64,
            'bias_init': zeros,
            'kernel_init': self.nonneg_init,
        }
        q_hidden_layer_kwargs = {
            'dtype': jnp.float64,
            'kernel_init': self.nonneg_init,
            'use_bias': False,
        }

        self.nonneg_i = 0
        q = x
        for num in self.layer_sizes:
            x = nn.Dense(num, **hidden_layer_kwargs, name=f'nonnegative_{self.nonneg_i}')(x); self.nonneg_i += 1
            y = nn.Dense(num, **q_hidden_layer_kwargs, name=f'nonnegative_{self.nonneg_i}')(q); self.nonneg_i += 1
            x = nn.sigmoid(x + y)
        x = nn.Dense(self.num_outputs, **hidden_layer_kwargs, name=f'nonnegative_{self.nonneg_i}')(x); self.nonneg_i += 1
        y = nn.Dense(self.num_outputs, **q_hidden_layer_kwargs, name=f'nonnegative_{self.nonneg_i}')(q); self.nonneg_i += 1
        x = nn.softplus(x + y)
        return x

class FIQCNN(nn.Module):
    """A quasi-convex version of a FICNN"""

    num_outputs: int
    convex_outputs: int
    convex_layer_sizes: List[int]
    monotonic_layer_sizes: List[int]

    @nn.compact
    def __call__(self, x):
        x = FICNN(self.convex_outputs, self.convex_layer_sizes)(x)
        x = PositiveMonotonicNN(self.num_outputs, self.monotonic_layer_sizes)(x)
        return x

class MLP(nn.Module):
    """Just a simple multi-layer network."""

    num_outputs: int
    output_activation: str
    layer_sizes: List[int]

    def setup(self):
        self.normal_init = uniform(-3e-1, 3e-1)

    @nn.compact
    def __call__(self, x):
        inputs = x
        hidden_layer_kwargs = {
            'kernel_init': self.normal_init,
            'bias_init': zeros,
            'dtype': jnp.float64,
        }

        for num in self.layer_sizes:
            x = nn.Dense(num, **hidden_layer_kwargs)(x)
            x = nn.tanh(x)

        output_layer_kwargs = {
            'kernel_init': self.normal_init,
            'bias_init': zeros,
            'dtype': jnp.float64,
        }
        x = nn.Dense(self.num_outputs, **output_layer_kwargs)(x)
        if self.output_activation != 'linear':
            x = getattr(nn, self.output_activation)(x)

        if self.num_outputs == 1:
            x = x.reshape((-1,))
        return x
