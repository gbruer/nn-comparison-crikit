from crikit.fe import *
from crikit.fe_adjoint import *

from crikit import CR, CR_UFL_Expr, JAXArrays, PointMap, get_invariant_functions, project
from crikit.cr.space_builders import DirectSum
from pyadjoint.overloaded_type import create_overloaded_object as coo
from pyadjoint_utils import *
from pyadjoint_utils.jax_adjoint import overload_jax

import crikit.utils as utils
import jax
import jax.numpy as jnp
import numpy as onp

class ParamsInterface():
    def set_params(self, params, bounds=None):
        # super(ParamsInterface, self).set_params(params)
        self._params = params
        self._params_bounds = bounds
        self._params_controls = [Control(p) for p in self._params]

    def get_params_numpy(self):
        params_list = []
        for i, c in enumerate(Enlist(self._params_controls)):
            params_list += c.fetch_numpy(c.control)
        return onp.array(params_list, dtype="d")

    def set_params_numpy(self, params_array):
        """set_params() must be called before this is called."""
        m = [control.copy_data() for control in self._params_controls]
        offset = 0
        for i, control in enumerate(self._params_controls):
            m[i], offset = control.assign_numpy(m[i], params_array, offset)
        self.set_params(m, bounds=self._params_bounds)

    @property
    def params(self):
        return self._params

    @property
    def params_bounds(self):
        return self._params_bounds

    @property
    def params_controls(self):
        return self._params_controls


class ParamsCR(ParamsInterface, CR):
    @property
    def h(self):
        h = [coo(jnp.array(1e-1*onp.random.rand(*p.shape))) for p in self.params]
        return h

class ParamsCRUFL(ParamsInterface, CR_UFL_Expr):
    pass

class SPDIntegratorCR(ParamsCR):
    def __init__(self, *args, **kwargs):
        if len(args) >= 3:
            self.G_func = args[2]
        else:
            self.G_func = kwargs.get('cr_function')
        self.integration_mode = kwargs.get('integration_mode', 'gaussian-quadrature')

        super().__init__(*args, **kwargs)
        # self.G_func = self._f
        self._scalar_invt_func_unvmapped,_ = get_invariant_functions(self._invariant_info,suppress_warning_print=True)
        self.function = self._make_f()


    @property
    def function(self):
        return self._f

    @function.setter
    def function(self,f):
        if isinstance(f,str):
            self.load_tensorflow_model(f)
        else:
            self._f = f
        self._f = jax.vmap(self._f,in_axes=self._vmap_axes)
        self._overloaded_call = self._make_overloaded_call()

    def _make_f(self):
        dim = self._invariant_info.spatial_dims

        if dim == 3:
            tensor_to_voigt_vector = (jnp.array([0,1,2,1,0,0]), jnp.array([0,1,2,2,2,1]))
        elif dim == 2:
            tensor_to_voigt_vector = (jnp.array([0, 1, 0]), jnp.array([0, 1, 1]))
        else:
            raise ValueError(f'Bad dimension: {dim}')
        def strain_to_voigt_vector(eps):
            eps_vec = eps[tensor_to_voigt_vector]
            eps_vec2 = jnp.concatenate((eps_vec[:dim], 2*eps_vec[dim:]))
            return eps_vec2

        def voigt_vector_to_tensor(vec):
            if dim == 3:
                return jnp.array([[vec[0], vec[5], vec[4]],
                                  [vec[5], vec[1], vec[3]],
                                  [vec[4], vec[3], vec[2]]])
            elif dim == 2:
                return jnp.array([[vec[0], vec[2]],
                                  [vec[2], vec[1]]])
            raise ValueError(f'Bad dimension: {dim}')

        def eval_func(inputs, *params):
            eps = inputs[0]
            def integrand_func(t):
                """We need to integrate this function from t=0 to t=1."""
                eps_prime = t * eps
                scalar_invariants = self._scalar_invt_func_unvmapped(eps_prime)
                G = self.G_func(scalar_invariants, *params)
                return G

            eps_vec = strain_to_voigt_vector(eps)
            if self.integration_mode == 'gaussian-quadrature':
                points, weights = onp.polynomial.legendre.leggauss(deg=3)
                range_scale = 0.5

                integral = sum(w*integrand_func((p+1)/2) for p,w in zip(points, weights))
                sigma_vec = range_scale * (integral @ eps_vec)
            elif self.integration_mode == 'left-riemann-sum':
                ts, dt = onp.linspace(0, 1, 10, endpoint=False, retstep=True)
                # integral = dt * sum(integrand_func(t) for t in ts)
                integral = dt * jnp.sum(jnp.array([integrand_func(t) for t in ts]), axis=0)
                sigma_vec = integral @ eps_vec
            elif self.integration_mode == 'none':
                sigma_vec = integrand_func(1) @ eps_vec
            else:
                raise ValueError(f'Invalid integration mode: {self.integration_mode}')
            sigma = voigt_vector_to_tensor(sigma_vec)
            return sigma

        return eval_func

    def _make_overloaded_call(self):
        if self._vmap:
            pointwise = tuple(ax is not None for ax in self._vmap_axes)
        else:
            pointwise = False
        overloaded = overload_jax(self._f, nojit=self._nojit,
                                  function_name=f'IntegratorCR({self._f.__name__})',
                                  argnums=self._diff_argnums,
                                  static_argnums=self._static_argnums,
                                  pointwise=pointwise)
        return overloaded

def jax_ppos(x):
    return (x+jnp.abs(x))/2.

def jax_as_voigt_tensor(X):
    return jnp.array([[X[0], X[2]],
                      [X[2], X[1]]])

def jax_as_voigt_vector(X):
    return jnp.array([X[0, 0], X[1, 1], X[0, 1]])

def jax_dev(x):
    n = x.shape[0]
    return x - jnp.trace(x)/n * jnp.identity(n)

class PlasticityCR(PointMap, ParamsInterface):
    def __init__(self, E, nu, yield_stress, tangent_modulus=None, hardening_modulus=None):
        """
            This is a 2D plasticity CR for plane stress.

            This CR takes in the tensors as vectors. I.e., the 2D strain should be a vector of length 3.

            yield_function and elastic_cr must be JAX functions that have not been vmapped yet.
            elastic_cr must be linear.
        """

        # Elastic params.
        self.E = coo(E)
        self.nu = coo(nu)

        @overload_jax
        def get_C_mu(E, nu):
            s = E / (1 - nu*nu)
            mu = E / (2 * (1 + nu))

            C = jnp.array([
                    [s, s * nu, 0],
                    [s * nu, s, 0],
                    [0,      0, mu],
                ])
            return C, mu

        self.C, self.mu = get_C_mu(self.E, self.nu)
        self.C.tf_name = 'C'
        self.mu.tf_name = 'mu'

        # Plastic params.
        self.yield_stress = coo(yield_stress)
        if tangent_modulus is None and hardening_modulus is None:
            raise ValueError('Missing tangent_modulus or hardening_modulus')
        if tangent_modulus is not None and hardening_modulus is not None:
            raise ValueError('Must specify either tangent_modulus or hardening_modulus but not both')

        if tangent_modulus is None:
            self.H = coo(hardening_modulus)
        else:
            @overload_jax
            def get_hardening_modulus(E, tangent_modulus):
                return E * tangent_modulus / (E - tangent_modulus)
            self.H = get_hardening_modulus(self.E, tangent_modulus)

        # Set up elasto-plastic functions.
        def elastic_cr(strain, C):
            return C @ strain

        def yield_function(stress, plasticity, yield_stress, H):
            s1, s2, s3 = stress
            vonMises = jnp.sqrt(s1**2 - s1*s2 + s2**2 + 3*s3**2)
            return vonMises - yield_stress - H*plasticity

        self.elastic_cr = elastic_cr
        self.f = yield_function

        self._extra_args = (self.C, self.mu, self.H, self.yield_stress)

        self.df = jax.value_and_grad(self.f, argnums=0)
        self.do_plasticity = True
        self.reset_hidden_state()

        tensor_shape = (-1, 3,)
        in_shapes = (tensor_shape, tensor_shape, tensor_shape)
        out_shape = tensor_shape

        source = DirectSum(tuple(JAXArrays(s) for s in in_shapes))
        target = JAXArrays(out_shape)
        super().__init__(source, target)

        pointwise = (True,True,True,True) + (False,)*len(self._extra_args)
        in_axes = tuple(0 if pw else None for pw in pointwise)
        self._get_stress_plastic_vmapped = jax.vmap(self._get_stress_plastic_point, in_axes=in_axes)
        self._overloaded_call = overload_jax(self._get_stress, pointwise=pointwise, nojit=False, checkpoint=False, concrete=False)

    def update_hidden_state(self, point):
        stress, self.plasticity = self._overloaded_call(*point, self.plasticity, *self._extra_args)
        # print('plasticity norm:', onp.sqrt(jnp.sum(self.plasticity.value*self.plasticity.value)))
        return stress

    def reset_hidden_state(self):
        self.plasticity = None

    def __call__(self, point):
        if self.plasticity is None:
            # Initialize plasticity array.
            self.plasticity = create_overloaded_object(jnp.zeros(point[0].shape[0]))
        stress, plasticity = self._overloaded_call(*point, self.plasticity, *self._extra_args)
        stress.tf_name = 'stress'
        plasticity.tf_name = 'plasticity'
        return stress

    def _get_stress(self, strain, old_strain, old_stress, plasticity, *extra_args):
        stress, dp = self._get_stress_plastic_vmapped(strain, old_strain, old_stress, plasticity, *extra_args)
        return stress, plasticity + dp

    def _get_stress_plastic_point(self, strain, old_strain, old_stress, plasticity, *extra_args):
        C, mu, H, yield_stress = extra_args

        # Elastic predictor.
        elastic_stress_vec = old_stress + self.elastic_cr(strain - old_strain, C)
        if not self.do_plasticity:
            return elastic_stress_vec, 0

        # Plastic correction.
        elastic_stress = jax_as_voigt_tensor(elastic_stress_vec)
        s = jax_dev(elastic_stress)
        von_mises = jnp.sqrt(3/2 * jnp.sum(s*s))
        f = self.f(elastic_stress_vec, plasticity, yield_stress, H)
        dp = jax_ppos(f)/(3*mu + H)

        beta = 3*mu*dp/von_mises
        corrected_stress = elastic_stress - beta * s
        corrected_stress_vec = jax_as_voigt_vector(corrected_stress)
        return corrected_stress_vec, dp

    def _archived_get_stress_plastic_point(self, strain, old_strain, old_stress, plasticity):
        stress0 = old_stress + self.elastic_cr(strain - old_strain)
        # Commented out attempt to use jax.lax to make it jittable.
        # elastic_check = (self.f(stress0, plasticity) <= 0)
        # stress = jax.lax.cond(elastic_check, lambda x: x[0], self._newton_solve_loop, (stress0, plasticity))
        if True or self.f(stress0, plasticity) <= 0:
            return stress0, 0
        stress, dp = self._newton_solve_loop((stress0, plasticity))
        return stress, dp

    def _newton_solve_loop(self, args):
        stress0, plasticity = args
        def residual(stress, dp):
            f, df_dstress = self.df(stress, plasticity + dp)
            r_stress = stress - stress0 + dp * self.elastic_cr(df_dstress)
            r_dp = f
            return jnp.concatenate((r_stress, r_dp[None]))
        residual_jac_jax = jax.jacrev(residual, argnums=(0,1))
        def residual_jac(stress, dp):
            J_stress, J_dp = residual_jac_jax(stress, dp)
            return jnp.concatenate((J_stress, J_dp[:, None]), axis=1)

        Newton_maxiter = 10
        Newton_Abs_Err = 1e-8
        Newton_Rel_Err = 1e-5

        dp = 0.
        stress = stress0

        res0 = residual(stress, dp)
        res0_norm = jnp.linalg.norm(res0)

        for i in range(Newton_maxiter):
            res = residual(stress, dp)
            J = residual_jac(stress, dp)
            step = -jnp.linalg.solve(J, res)
            stress += step[0:3]
            dp += step[3]
            res_norm = jnp.linalg.norm(res)
            if res_norm/res0_norm < Newton_Rel_Err or res_norm < Newton_Abs_Err:
                break
        else:
            raise RuntimeError("Newton failed to converge")

        # Commented out attempt to use jax.lax to make it jittable.
        # def cond_fun(val):
        #     stress, dp, res_norm, i = val
        #     return res_norm/res0_norm < Newton_Rel_Err or res_norm < Newton_Abs_Err or i >= Newton_maxiter

        # def body_fun(val):
        #     stress, dp, res_norm, i = val

        #     res = residual(stress, dp)
        #     J = residual_jac(stress, dp)
        #     step = -jnp.linalg.solve(J, res)
        #     stress += step[0:3]
        #     dp += step[3]
        #     res_norm = jnp.linalg.norm(res)

        #     return stress, dp, res_norm, i + 1

        # init_val = (stress, dp, res0_norm, 0)
        # stress, dp, res_norm, i = jax.lax.while_loop(cond_fun, body_fun, init_val)
        # if i >= Newton_maxiter:
        #     raise RuntimeError("Newton failed to converge")

        return stress, dp

    def _archived_get_stress_plastic_point(self, strain, old_strain, old_stress, plasticity):
        stress0 = old_stress + self.elastic_cr(strain - old_strain)

        if self.f(stress0, plasticity) <= 0:
            return stress0

        def residual(stress, dp):
            f, df_dstress = self.df(stress, plasticity + dp)
            r_stress = stress - stress0 + dp * self.elastic_cr(df_dstress)
            r_dp = f
            return jnp.concatenate((r_stress, r_dp))
        residual_jac_jax = jax.jacrev(residual, argnums=(0,1))
        def residual_jac(stress, dp):
            Js = residual_jac_jax(stress, dp)
            return jnp.concatenate(Js)

        Newton_maxiter = 10
        Newton_Abs_Err = 1e-8
        Newton_Rel_Err = 1e-5

        dp = 0
        stress = stress0

        res0 = residual(stress, dp)
        res0_norm = jax.linalg.norm(res0)
        for i in range(Newton_maxiter):
            res = residual(stress, dp)
            J = residual_jac(stress, dp)
            step = -jnp.solve(J, res)
            stress += step[0:2]
            dp += step[3]

            res_norm = jax.linalg.norm(res)
            if res_norm/res0_norm < Newton_Rel_Err or res_norm < Newton_Abs_Err:
                break
        else:
            raise RuntimeError("Newton failed to converge")
        return stress


def ufl_ppos(x):
    return (x+abs(x))/2.

def ufl_as_voigt_tensor(X):
    return as_tensor([[X[0], X[2]],
                      [X[2], X[1]]])

def ufl_as_voigt_vector(X):
    return as_vector([X[0, 0], X[1, 1], X[0, 1]])

class UFLPlasticity():
    """Warning: when using plasticity, this will give NaNs under certain values. E.g., if all the inputs are 0."""

    def __init__(self, E, nu, yield_stress, tangent_modulus=None, hardening_modulus=None, plasticity_function_space=None):
        # Elastic params.
        self.E = E
        self.nu = nu

        self.s = self.E / (1 - self.nu*self.nu)
        self.mu = self.E / (2 * (1 + self.nu))
        self.C = as_tensor([
                [self.s, self.s * self.nu, 0],
                [self.s * self.nu, self.s, 0],
                [0,      0, self.mu],
            ])

        # Plastic params.
        self.yield_stress = yield_stress

        if tangent_modulus is None and hardening_modulus is None:
            raise ValueError('Missing tangent_modulus or hardening_modulus')
        if tangent_modulus is not None and hardening_modulus is not None:
            raise ValueError('Must specify either tangent_modulus or hardening_modulus but not both')

        if tangent_modulus is None:
            self.H = hardening_modulus
        else:
            self.H = self.E * tangent_modulus / (self.E - tangent_modulus)

        self.do_plasticity = (plasticity_function_space is not None)
        if self.do_plasticity:
            self.plasticity_function_space = plasticity_function_space
            self.reset_hidden_state()

    def __call__(self, point):
        stress_vec, dp = self._compute_stress_plasticity(*point)
        return stress_vec

    def _compute_stress_plasticity(self, strain_vec, old_strain_vec, old_stress_vec):
        # Elastic prediction.
        dstrain_vec = strain_vec - old_strain_vec
        dstress_vec = dot(self.C, dstrain_vec)
        elastic_stress_vec = old_stress_vec + dstress_vec
        if not self.do_plasticity:
            return elastic_stress_vec, None

        # Plastic correction.
        elastic_stress = ufl_as_voigt_tensor(elastic_stress_vec)
        s = dev(elastic_stress)
        von_mises = sqrt(3/2 * inner(s,s))
        f = von_mises - self.yield_stress - self.H*self.plasticity
        dp = ufl_ppos(f)/(3*self.mu + self.H)
        beta = 3*self.mu*dp/von_mises
        corrected_stress = elastic_stress - beta * s
        corrected_stress_vec = ufl_as_voigt_vector(corrected_stress)
        return corrected_stress_vec, dp

    def update_hidden_state(self, point):
        stress_vec, dp = self._compute_stress_plasticity(*point)
        if self.do_plasticity:
            dp_func = project(dp, self.plasticity.function_space())
            self.plasticity.assign(self.plasticity + dp_func)
            # print('plasticity norm:', self.plasticity.vector().norm('l2'))
        return stress_vec

    def reset_hidden_state(self):
        if self.do_plasticity:
            self.plasticity = Function(self.plasticity_function_space)
